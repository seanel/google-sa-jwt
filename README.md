# Google SA JWT

A simple utility for obtaining a Google signed OIDC JWT based on service account credentials with Cloud Run endpoints that do not allow unauthenticated requests.

## Pre-Requisites

* The service account should be created and have the role `Cloud Run Invoker`
* Create a JSON key for the service account and have it available locally or in the context of where you are running this from

## Usage

Two environment variables should be exported that provide the path to the service account's JSON key and the Cloud Run URL for the service you wish to authenticate against:

```bash
export GOOGLE_APPLICATION_CREDENTIALS="/path/to/sa.json"
export CLOUD_RUN_SERVICE_URL="https://somecloudrunservice-abcdefghi-lz.a.run.app"
```

Then you can get the credential as follows:

```bash
pip3 install -r requirements.txt
python3 create_jwt_.py
```

This will print out the signed JWT ready to use! If you wish to capture it in an environment variable for use elsewhere you could run instead:

```bash
JWT=$(python3 create_jwt.py)
```
